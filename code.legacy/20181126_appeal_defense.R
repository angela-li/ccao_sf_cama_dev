# Comments ----

# This script examines the case for an appeal of a given property

# Top -----
rm(list=ls());flush.console(); theme_set(theme_bw());

# Controls ----
# Who is running this code?
user <- "Robert Ross"

# What packages will you need?
libs <- c("xlsx", "maptools","sp","foreign","fields", "mgcv", "car","RPostgreSQL"
          , "plyr","dplyr", "DBI", "RODBC", "RStata", "lattice", "RColorBrewer", 
          "ggplot2", "scales", "zoo", "leaflet", "htmlwidgets", "htmltools", "sjPlot"
          ,"gbm", "progress", "knitr", "kableExtra")

# What data to use? (data named modeldata_`data_id')
data_id <- "73"

# What other scripts need to be loaded?
scripts <-c("2018115_reopen_HP&W_functions.r")

# Where are your files stored?
if(user=="Robert Ross"){setwd("C:/Users/Robert A Ross/Dropbox/Chicago Triad Assessment Data");}
if(user=="A new user"){setwd("---------SET ME----------");}
dirs <- list(code="C:/Users/Robert A Ross/Documents/CCAO_CAMA_SF/code.r/"
             , data="C:/Users/Robert A Ross/Dropbox/Chicago Triad Assessment Data/data/"
             , results="C:/Users/Robert A Ross/Dropbox/Chicago Triad Assessment Data/results/"
             , data_shp="C:/Users/Robert A Ross/Documents/CCAO_CAMA_SF/data/SHP/shp2016.Rdata"
             , data_shp_raw="data/SHP/ccgisdata_-_Parcels_2016.csv");
options(scipen=20);


# Refresh ----
refresh <- function(recode=TRUE){
  # Refresh packages
  check.packages <- function(libs){
    new.libs <- libs[!(libs %in% installed.packages()[, "Package"])]
    if (length(new.libs)) 
      install.packages(new.libs, dependencies = TRUE)
    sapply(libs, require, character.only = TRUE)
  }
  for (iter in 1:length(libs))
    check.packages(libs[iter]);
  
  #Refresh functions
  for (iter in 1:length(scripts)){
    source(paste0(dirs$code, scripts[iter]))
  }
  rm(iter)
}
refresh()

# stick all CSVs together to load into Postgres ----
# Note that you have to change the root directory in this file manually
# chooseStataBin()
# options("RStata.StataVersion" = 14)
# DO THIS ONCE
# stata(paste(dirs$code, "Paste files together.do", sep=""))


# ODBC Connect ----
drv <- dbDriver("PostgreSQL")
pw <- "password"

con <- dbConnect(drv, dbname = "postgres",
                 host = "localhost", port = 5432,
                 user = "postgres", password = pw)


# Load modeling data
load(file=paste0(dirs$data, "modeldata_", data_id, ".Rdata"))

# Sample target PIN
target_pin <- 14073040430000

# Describe target pin
head(subset
     (modeldata[c("pin", "X","Y", "sqftl", "sqftb", "class"
      , "age", "saledate","amount1", "rooms", "bedrooms", "fullbath", "halfbath")]
            , pin==target_pin))

# Calculate euclidean distances
modeldata$a <- modeldata$X-(-87.68698)
modeldata$b <- modeldata$Y-(41.97332)
modeldata$c <- sqrt(modeldata$a^2 + modeldata$b^2)
df <- subset(modeldata[c("pin", "saledate", "amount1", "X", "Y", "c")]
             , !is.na(X) 
             & !is.na(amount1)
             & !is.na(saledate)
             & saledate >= as.Date("2012-01-01", "%Y-%m-%d"
            ))


# Graph distance vs. time
ggplot(data=df, aes(x=df$saledate,y=df$c)) +
  geom_point()+ scale_x_date(labels = date_format("%m-%Y")) +
  geom_point(aes(x=as.Date("08/01/2016", "%m/%d/%Y"), y=0)
             , colour="light blue", size = 9)


# Find nearly identical properties using Euclidean distance

comps <- subset(modeldata
                , modeldata$sqftl>=3000-.05*3000
                & modeldata$sqftl<=3000+.05*3000
                & modeldata$sqftb>=1428-.05*1428
                & modeldata$sqftb<=1428+.05*1428
                & modeldata$bedrooms==4
                & modeldata$fullbath==2
                & modeldata$halfbath==0
                & modeldata$age>=50
                & saledate>=as.Date("01/01/2016", "%m/%d/%Y")
                & !is.na(LOCF) 
                & !is.na(amount1)
                )

comps$target <- target_pin
comps$target_id <- ifelse(comps$pin==target_pin,0,1)
comps$x_match <- 1

mean(comps$amount1)
hist(comps$amount1) 
  
tag.map.title <- tags$style(HTML("
 .leaflet-control.map-title { 
transform: translate(-50%,20%);
position: fixed !important;
left: 50%;
text-align: center;
padding-left: 10px; 
padding-right: 10px; 
background: rgba(255,255,255,0.75);
font-weight: bold;
font-size: 28px;
}
"))
factpal <- colorFactor(topo.colors(5), comps$target_id
                       , na.color = "#808080")
title <- tags$div(
  tag.map.title, HTML("Title"))
popup <- paste(sep = "<br/>"
               , "<b> PIN: </b>", comps$pin
               , "<b> Sale price: </b>", paste("$", prettyNum(comps$saleamt,big.mark=",",scientific=FALSE), sep="")
               , "<b> Sale date: </b>", comps$saledate
               , "<b> Property Class: </b>", comps$CLASS
               , "<b> Match </b>", comps$target_id
)
leaflet(comps) %>%
  addTiles() %>%
  addCircleMarkers( lat = comps$Y, lng =  comps$X
                    , color = ~factpal(comps$target_id), popup =  popup) %>% 
  addControl(title, position = "topleft")
