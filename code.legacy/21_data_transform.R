DT_clip <- function(scope) {
	U_msg("[DT_clip] Using flat outlier constants from the tyler paper\n");
	MODELDAT_FILTER <- list(
		minyear=2012,
		minmv=65000,maxmv=790000
		);

	MODELDAT_FULL <- scope$MODELDAT_FULL;
	
	modeldat_extremeMV <- subset(MODELDAT_FULL,mv<=MODELDAT_FILTER$minmv | mv>=MODELDAT_FILTER$maxmv);
	modeldat <- subset(MODELDAT_FULL,mv>MODELDAT_FILTER$minmv & mv<MODELDAT_FILTER$maxmv &
					!is.na(saledate) & puremarket==1 & as.numeric(format(saledate,"%Y"))>=MODELDAT_FILTER$minyear);	
	
	U_msg("\tKept ",nrow(modeldat)," sales in $modeldat, excluded ",nrow(modeldat_extremeMV)," in $modeldat_extremeMV\n");
	
	scope$MODELDAT_FILTER <- MODELDAT_FILTER;
	scope$modeldat_extremeMV <- modeldat_extremeMV;
	scope$modeldat <- modeldat;
		
	return(scope);
}

DT_createVariables <- function(scope) {
	U_msg("[DT_createVariables] Creating standard features for both '$modeldat' (modeling set) and '$regt' (full assessment set)\n");
	regt <- scope$REGT;
	modeldat <- scope$modeldat;

	createVars <- function(dat) {
		dat <- within(dat,{
				PPSF <- saleamt/sqftb;
				rqos <- 4*(2017-as.numeric(format(saledate,"%Y"))) + (5 - ceiling(as.numeric(format(saledate,"%m"))/3));
				rhos <- 2*(2017-as.numeric(format(saledate,"%Y"))) + (3 - ceiling(as.numeric(format(saledate,"%m"))/2));
				RQOS <- factor(rqos);
				RHOS <- factor(rhos);
				# Coded vars
				BSF <- sqftb;
				LSF <- sqftl;
				FULLBATH <- fullbath;
				ROOMS <- factor(rooms);
				NUM <- factor(num);	
				EXTCON <- factor(extcon);
				FIREPL <- factor(firepl);
				
				CLASS <- factor(class);
		});

		# Might as well remove the lower-case copy versions
			dat <- subset(dat,select=-c(sqftb,sqftl,fullbath,rooms,num,extcon,firepl,
										class));
		
		# Rearrange so that most important variables are in front, then the end stuffs...
			imp <- c("pin","X","Y","saleamt","mos","yr","BSF","LSF","FULLBATH","RQOS","RHOS", "ROOMS","NUM","EXTCON","FIREPL");
			rest <- setdiff(colnames(dat),imp);
			dat <- subset(dat,select=c(imp,rest));
	}
	
	modeldat <- createVars(scope$modeldat);
	regt <- createVars(scope$REGT);
		regt$mv <- NA;
		# regt rqos should be set to the final value present in the salesfile
		regt$rqos <- min(modeldat$rqos);
		regt$RQOS <- factor(regt$rqos,levels=levels(modeldat$RQOS));
		# Strip down the assessment set to the same variables for now
		regt <- subset(regt,select=colnames(modeldat));		
	
	# Copy back
		scope$regt <- regt;
		scope$modeldat <- modeldat;
		
	return(scope);
}

DT_createLOCF <- function(scope) {
	U_msg("[DT_createLOCF] Creating a 'location factor' in modeling dataset '$model', and applying onto '$regt'\n");
	regt <- scope$regt;
	modeldat <- scope$modeldat;

	# Estimate model, and put fitted values onto the sales data
	U_msg("\tEstimating model...\n");
	library(mgcv);
	LOCF_MODEL <- gam(log(saleamt)~s(X)+s(Y)+s(X,Y)+s(rqos),data=modeldat);
		# Estimate by forcing rqos to min
		this <- modeldat;
		this$rqos <- min(modeldat$rqos);
		this <- exp(predict(LOCF_MODEL,newdata=this));
		
		modeldat$LOCF <- NA;
		locf_center <- mean(this);
		modeldat$LOCF <- this/locf_center;
	
	# Also apply it onto the full xy set
	U_msg("\tApplying model onto all parcels with recent sales...\n");
		regt$rqos <- min(modeldat$rqos);
		regt$LOCF <- exp(predict(LOCF_MODEL,newdata=regt))/locf_center;

	
	scope$LOCF_MODEL <- LOCF_MODEL;
	scope$modeldat <- modeldat;
	scope$regt <- regt;
	return(scope);
}