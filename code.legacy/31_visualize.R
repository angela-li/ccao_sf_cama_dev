plot.scope <- function(scope,...) {
	U_msg("What would you like to visualize?\n");
	choices <- c("LOCF","Standard Diagnostics","Spatial Residuals","PIN Report");
	choice <- select.list(choices,title="Visualization");
	
	if (choice==choices[1]) {
		V_LOCF(scope,...);
	} else if (choice==choices[2]) {
		V_stddiagnostics(scope,...);
	} else if (choice==choices[3]) {
		V_spatialresid(scope,...);
	} else if (choice==choices[4]) {
		V_pinreport(scope,...);
	}
	return();
	
	if (FALSE) {
		# Print out information about a property
		U_msg("pin ",property$pin,"\n");
		U_msg("$",property$saleamt," (on ",property$mos,"/",property$yr,")\n");
		U_msg("SQFT: ",property$BSF," | Lot: ",property$LSF," | Bed: ",property$bedrooms," | Bath: ",property$FULLBATH,"\n");

		# Draw a map
		m <- leaflet();
		m <- addTiles(m);
		m <- addMarkers(m,lng=property$X,lat=property$Y,popup=this$pin);
		
		m
	}
}
V_LOCF <- function(scope,drawparcels=FALSE) {
	if (!("LOCF_MODEL"%in%names(scope)))
		stop("LOCF has not been run yet");

	LOCF_MODEL <- scope$LOCF_MODEL;
	LOCF <- subset(scope$modeldat,!is.na(LOCF),select=c(pin,X,Y,rqos,LOCF));
	regt <- scope$regt;
	if (drawparcels) {
		LOCF <- S_xyToPoly(scope,LOCF);
		regt <- S_xyToPoly(scope,regt);
	}
	
	# Define colorpoints and plot
	loccolors <- c("yellow","orange","red","purple","darkblue");
	quintiles <- quantile(regt$LOCF,probs=seq(0,1,by=.2),na.rm=TRUE);
	brk <- list(
			full=cut(regt$LOCF,breaks=quintiles,include.lowest=TRUE),
			sales=cut(LOCF$LOCF,breaks=quintiles,include.lowest=TRUE)
			);

	par(bg="black",col.axis="white",col.lab="white",col.main="white");
	plot(regt$X,regt$Y,col=loccolors[as.numeric(brk$full)],pch=".");
	if (drawparcels) {
		plot(LOCF,col=loccolors[as.numeric(brk$sales)],border=loccolors[as.numeric(brk$sales)],add=TRUE);
	} else {
		points(LOCF$X,LOCF$Y,col=loccolors[as.numeric(brk$sales)],pch="+");
	}
	title(paste0("Location Factor model for ",paste(scope$townships,collapse="+")));
	legend("top",legend=paste0(round(quintiles[-length(quintiles)],2),"-",round(quintiles[-1],2)),fill=loccolors,horiz=TRUE,bg="lightyellow",cex=.7);
	box(col="white");
}

V_stddiagnostics <- function(scope,modelname=NULL,panels=NULL) {
	U_msg("Plotting range of diagnostics\n");
	require(RColorBrewer);
	require(car);
	
	# Determine what to draw diagnostics for
	if (TRUE) {
		if (!is.null(modelname)) {
			modelname <- match.arg(modelname,names(scope$modelobjects));
		} else {
			choices <- c(names(scope$modelobjects), "All models");
			modelname <- select.list(choices,title="Model");
		}
		# And which plots to draw
		if (!is.null(panels)) {
			panels <- intersect(panels,1:4);
		} else {
			choices <- c("1: Summary Table","2: Ratios","3: COD/PRD Ranges","4: Suites Curve");
			panels <- select.list(choices,title="Plot Panels",preselect=choices,multiple=TRUE);
			panels <- as.numeric(substr(panels,1,1));
		}
	}
	
	# Extract the diagnostic objects from the object
	diags <- scope$diagnostics[[modelname]];
	ratios <- attr(diags,"ratios");
	boots <- attr(diags,"boot");
		boots <- do.call("rbind",boots);
	suitesraw <- attr(diags,"suitesraw");
	
	# Layout the plots
	npanels <- length(panels);
	this_mfrow <- switch(npanels,"1"=c(1,1),"2"=c(1,2),"3"=c(2,2),"4"=c(2,2));
	par(mfrow=this_mfrow,xaxs="i",yaxs="i",las=1);

	if (1%in%panels) V_std1(diags,modelname=modelname);
	if (2%in%panels) V_std2(diags,ratios,modelname=modelname);
	if (3%in%panels) V_std3(diags,boots,modelname=modelname);
	if (4%in%panels) V_std4(diags,suitesraw,modelname=modelname);
}
# 'stddiagnostics' relies on individual components defined below
	V_std1 <- function(diags,modelname="Unknown") {
		plot(NA,NA,xlim=c(-1,1),ylim=c(-1,1),xlab="",ylab="",axes=FALSE,main=paste0("Summary Table for model ",modelname));
			text(-.8,.95,paste0("IQR = ",diags$IQR[1]));
			cols1 <- c("IQRm","count","ratLO","ratHI","median","mean","wgtmean");
			cols2 <- c("IQRm","count","R2","COD","PRD","PRB","suites");
			.xat <- seq(-.8,.8,len=length(cols1));
			.yat1 <- seq(.8,.2,len=nrow(diags)+1);
			.yat2 <- seq(-.2,-.8,len=nrow(diags)+1);
			# Text
			for (iter in seq_along(cols1)) {
				text(.xat[iter],.yat1[1],cols1[iter],font=2);
				text(.xat[iter],.yat1[-1],diags[[cols1[iter]]]);
			}
			for (iter in seq_along(cols2)) {
				text(.xat[iter],.yat2[1],cols2[iter],font=2);
				text(.xat[iter],.yat2[-1],diags[[cols2[iter]]]);
			}
	}
	V_std2 <- function(diags,ratios,modelname="Unknown") {
		# Residuals
		brew <- brewer.pal(8,"Dark2")[1:nrow(diags)];

		this <- ratios;
		this <- cbind(this,ratio=this[,"Yhat"]/this[,"Y"]);
		this <- this[order(-this[,"ratio"]),];
		
		plot(this[,"ratio"],type='l',xlab="Observation, sorted by decreasing Ratio",ylab="Ratios",log='y',main=paste0("Sorted Ratios with IQR Cutoffs for model ",modelname));
		for (iter in 1:nrow(diags)) {
			flag <- this[,paste0("IQRm_",diags$IQRm[iter])];
			that <- this[which(flag==1),];
			abline(h=range(that[,"ratio"]),col=brew[iter],lwd=sqrt(iter));
		}
		legend("top",legend=diags$IQRm,col=brew,lwd=1,bg="lightyellow",title="IQR Cutoff",horiz=TRUE,cex=.7);
	}
	V_std3 <- function(diags,boots,modelname="Unknown") {
		brew <- brewer.pal(8,"Dark2")[1:nrow(diags)];
		# Ellipse plots of main statistics
		plot(NA,NA,xlim=c(0,30),ylim=c(.98,1.1),xlab="COD",ylab="PRD",main=paste0("COD/PRD Ranges for model ",modelname));
		mtext("Point and Confidence Ranges",3);
		for (iter in 1:nrow(diags)) {
			this <- diags[iter,];
			those <- subset(boots,IQRm==this$IQRm);
			that <- dataEllipse(those$COD,those$PRD,levels=.95,draw=FALSE);
		
			polygon(that[,1],that[,2],col=paste0(brew[iter],"44"));
			#points(those$COD,those$PRD,col='darkgray');
			points(this$COD,this$PRD,col=brew[iter],pch=19,cex=3);
		}
		abline(h=axTicks(2),col='lightgray',lty='dotted');
		abline(v=axTicks(1),col='lightgray',lty='dotted');
		legend("topleft",legend=c(diags$IQRm),fill=brew,bg="lightyellow",title="IQR Cutoff");
		box();
	}
	V_std4 <- function(diags,suitesraw,modelname="Unknown") {
		brew <- brewer.pal(8,"Dark2")[1:nrow(diags)];
		# Suites plot
		plot(NA,NA,xlim=c(0,1),ylim=c(0,1),xlab="Cumulative saleamt",ylab="Cumulative prediction",main=paste0("Suites Curves for model ",modelname));
		abline(a=0,b=1,col='black');
		for (iter in 1:nrow(diags)) {
			points(suitesraw[[iter]][,1],suitesraw[[iter]][,2],col=brew[iter],type='l');
		}
		legend("bottomright",legend=c(diags$IQRm),fill=brew,bg="lightyellow",title="IQR Cutoff");
	}
	
V_spatialresid <- function(scope,modelname=NULL,drawparcels=FALSE) {
	require(RColorBrewer);
	if (!is.null(modelname)) {
		modelname <- match.arg(modelname,names(scope$modelobjects));
	} else {
		choices <- names(scope$modelobjects);
		modelname <- select.list(choices,title="Model");
	}
	
	# Get the model output, and the residual
	modeldat <- scope$modeldat;
	assessed <- scope$assessed;	# All properties
		if (!(modelname%in%colnames(modeldat))) stop("Fatal error: predictions for model ",modelname," not found");
		modeldat$pred <- modeldat[[modelname]];
		modeldat <- subset(modeldat,select=c(pin,X,Y,saleamt,pred));
	
		modeldat$presid <- modeldat$pred/modeldat$saleamt-1;
		if (drawparcels) {
			assessed <- S_xyToPoly(scope,assessed);
			modeldat <- S_xyToPoly(scope,modeldat);
		}
		
		
	# Define colorpoints and plot
	loccolors <- rev(brewer.pal(11,"PuOr")[c(2,4,6,8,10)]);
	brk <- cut(modeldat$presid,breaks=c(-1,-.25,-.15,.15,.25,1));
	par(bg="black",col.axis="white",col.lab="white",col.main="white");
	if (drawparcels) {
		plot(assessed,col='#252525');
		plot(modeldat,col=loccolors[as.numeric(brk)],add=TRUE);
	} else {
		plot(assessed$X,assessed$Y,col='darkgray',pch='.');
		points(modeldat$X,modeldat$Y,col=loccolors[as.numeric(brk)],pch=19);
	}
	title(paste0("[",modelname,"] model: % residuals for ",paste(scope$townships,collapse="+")));
	legend("top",legend=c("More 25% under","15-25% under","within 15%","15% over","More 25% over"),fill=loccolors,horiz=TRUE,bg="lightyellow",cex=.7,
				title="Predict vs. saleamt");
	box(col="white");
}
V_pinreport <- function(scope,pin=NULL) {
	if (is.null(pin)) {
		choices <- scope$assessed$pin;
		choices <- paste0(choices,ifelse(choices%in%scope$modeldat$pin," - RECENT SALE",""));
		choice <- select.list(choices,title="PIN");
		# Strip out the 'recent sale' part of the text
			pin_select <- strsplit(choice," ")[[1]][1];
	} else {
		if (!inherits(pin,"character")) pin <- as.character(pin);
		pin_select <- match.arg(pin,scope$assessed$pin);
	}
	
	# Locate the model value
	ix <- match(pin_select,scope$modeldat$pin);
		modeldat <- subset(scope$modeldat,select=c(pin,X,Y,saleamt,rqos))[ix,];
		comps_model <- scope$modelobjects$COMPS[[ix]];	
	# And the assessment value
	regt <- subset(scope$regt,pin==pin_select);
	ix <- match(pin_select,scope$assessed$pin);
		assessed <- scope$assessed[ix,];
		comps <- attr(scope$assessed,"COMPS")[[ix]];
	
	U_hr();
	U_msg("Assessment values, with comps\n");
		print(assessed);
		print(comps);
	U_hr();
	U_msg("Sales value and comps (if available in modeling set)\n");
		print(modeldat);
		print(comps_model);
	U_hr();
	U_msg("Property characteristics\n");
		print(regt);

}






