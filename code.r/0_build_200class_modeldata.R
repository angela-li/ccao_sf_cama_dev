# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/. 

# Top comments ----

# This script compiles data for modeling 200-class property values. The script assumes the user
# has a pre-defined directory structure on their local terminal, and is operating
# from within CCAO's network. Modification may be required for others.

# Version 0.2
# Written 12/22/2018
# Updated 1/16/2018 
# Updated XXXXXXXXX

load(destfile)

# /* This recode was moved into the R environment, but I'm leaving it here as a note for clarity.
#   , CASE WHEN BLDG_SF IS NULL THEN 1000 ELSE BLDG_SF END AS BLDG_SF 
#  This recode was moved into the R environment, but I'm leaving it here as a note for clarity.
#   , CASE WHEN total_bldg_sf IS NOT NULL THEN BLDG_SF/total_bldg_sf*sale_price
#     ELSE sale_price END AS sale_price To distribute sale price proportionally among the buildings
#  This recode was moved into the R environment, but I'm leaving it here as a note for clarity.
# , CASE WHEN MULTI_IND=1 THEN HD_HD_SF*(BLDG_SF/total_bldg_sf)  
#          WHEN MULTI_IND=0 THEN HD_HD_SF 
#          ELSE NULL 
#          END AS HD_SF 
#   */

# SQL Pull----
t <-  Sys.time()  
modeldata <- 
# result =  tryCatch( 
  dbGetQuery(CCAODATA, 
paste0("
SELECT 
/* Fields from HEAD */
HD_PIN as PIN, HD_CLASS as CLASS, H.TAX_YEAR, HD_NBHD AS NBHD, HD_HD_SF AS HD_SF
, CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) as TOWN_CODE
/* Fields from CCAOSFCHARS */
, CASE WHEN TYPE_RESD=0 THEN NULL ELSE TYPE_RESD END AS TYPE_RESD /* NULL recorded as 0 in AS400 */
, APTS, EXT_WALL, ROOF_CNST, ROOMS, BEDS, BSMT, BSMT_FIN, HEAT, OHEAT, AIR, FRPL, ATTIC_TYPE
, CASE WHEN ATTIC_FNSH=0 THEN NULL ELSE ATTIC_FNSH END AS ATTIC_FNSH /* NULL recorded as 0 in AS400 */
, HBATH, TP_PLAN, TP_DSGN, CNST_QLTY
, CASE WHEN RENOVATION=0 THEN NULL ELSE RENOVATION END AS RENOVATION /* NULL recorded as 0 in AS400 */
, SITE, GAR1_SIZE, GAR1_CNST, GAR1_ATT, GAR1_AREA, GAR2_SIZE, GAR2_CNST, GAR2_ATT  
, GAR2_AREA 
, CASE WHEN PORCH=0 THEN NULL ELSE PORCH END AS PORCH /* NULL recorded as 0 in AS400 */
, OT_IMPR, BLDG_SF, REPAIR_CND, MULTI_CODE, VOLUME, NCU
/* Fields from BRMASTER, Board of Review final figures */
, (LANDVAL)*10 as PRI_EST_LAND, (BLDGVAL)*10 as PRI_EST_BLDG
/* Fields from IDORSALES */
, sale_date, DOC_NO, sale_price
/* Location data from PINGEO */
, centroid_x, centroid_y, TRACTCE
/* Calculated field from CCAOSFCHARS where MULTI_IND==1 */
, total_bldg_sf
/* Recoded fields */
 , 1 as cons
  , CASE WHEN MULTI_IND IS NULL THEN 0 ELSE MULTI_IND END AS MULTI_IND
/* Account for missing addresses, then construct convenient property address field. */
  , CASE WHEN PL_HOUSE_NO IS NULL THEN 'ADDRESS MISSING FROM PROPLOCS' ELSE
  LTRIM(RTRIM(CAST(PL_HOUSE_NO as varchar(10)))) + ' ' + LTRIM(RTRIM(PL_DIR))+ ' ' + LTRIM(RTRIM(PL_STR_NAME))+ 
' ' + LTRIM(RTRIM(PL_STR_SUFFIX)) + ' ' + LTRIM(RTRIM(PL_CITY_NAME)) END as ADDR
/* Assign properties to modeling groups */
  , CASE WHEN HD_CLASS IN (200, 201, 241, 299) THEN 'NCHARS'
    WHEN HD_CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295) THEN 'SF' 
    WHEN HD_CLASS IN (211, 212) THEN 'MF'
    ELSE NULL END AS modeling_group
/* Account for missing characteristics for NCHARS group */
  , CASE WHEN FBATH IS NULL THEN 1 ELSE FBATH END AS FBATH
  , CASE WHEN AGE IS NULL THEN 10 ELSE AGE END AS AGE
  , CASE WHEN [USE] IS NULL THEN 1 ELSE [USE] END AS [USE]
/* Count number of condo units in a building */
  , n_units  
/* Where we have missing percentage of ownership, we divide values equally */
  , CASE WHEN DT_PER_ASS=0 THEN 1/n_units 
         WHEN DT_PER_ASS IS NULL THEN 1/n_units 
    ELSE DT_PER_ASS END AS PER_ASS 
/* Factor variable for NCHARS modeling group */
  , CASE WHEN HD_CLASS IN (200,201,241) THEN 200 
          WHEN HD_CLASS IN (299) THEN 299 
          ELSE NULL END AS CONDO_CLASS_FACTOR
/* Factor variable for mf modeling group */
  , CASE WHEN HD_CLASS IN (211, 212) THEN HD_CLASS 
          ELSE NULL 
          END AS MULTI_FAMILY_IND
/* 1 acre is an arbitrary break here */
  , CASE WHEN HD_HD_SF > 43559 THEN 1 
          ELSE 0 
          END AS LARGE_LOT  
FROM 
HEAD AS H /* The HEAD file defines the universe of PINs that could have a sale associated with them */
INNER JOIN /* Gives us each sale and the characteristics of the property IN THE YEAR it sold */
(SELECT B.PIN, B.RECORDED_DATE as sale_date, B.SALE_PRICE as sale_price, B.DOC_NO, B.DEED_TYPE 
, TYPE_RESD, [USE], APTS, EXT_WALL, ROOF_CNST, ROOMS, BEDS, BSMT, BSMT_FIN, HEAT, OHEAT, AIR, FRPL, ATTIC_TYPE
, ATTIC_FNSH, FBATH, HBATH, TP_PLAN, TP_DSGN, CNST_QLTY, RENOVATION, SITE, GAR1_SIZE, GAR1_CNST, GAR1_ATT, GAR1_AREA, GAR2_SIZE, GAR2_CNST, GAR2_ATT  
, GAR2_AREA, PORCH, OT_IMPR, BLDG_SF, REPAIR_CND, AGE, MULTI_CODE, VOLUME, NCU, MULTI_IND
FROM
  IDORSALES AS B
  INNER JOIN
    (SELECT DISTINCT A.PIN, A.RECORDED_DATE, A.SALE_PRICE, MAX(A.DOC_NO) AS DOC_NO FROM /* Arbitrarily select a deed */
    IDORSALES AS A
    INNER JOIN
      (SELECT X.PIN, X.RECORDED_DATE, MAX(X.SALE_PRICE) AS SALE_PRICE FROM /* Largest sale price by PIN and date */
        IDORSALES AS X
        GROUP BY X.PIN, X.RECORDED_DATE
      ) AS SSS
    ON A.PIN=SSS.PIN AND A.RECORDED_DATE=SSS.RECORDED_DATE AND A.SALE_PRICE=SSS.SALE_PRICE
    GROUP BY A.PIN, A.RECORDED_DATE, A.SALE_PRICE
    ) AS WW
  ON WW.PIN=B.PIN AND WW.RECORDED_DATE=B.RECORDED_DATE AND WW.DOC_NO=B.DOC_NO
  LEFT JOIN
    CCAOSFCHARS AS CC /* Get characeristics for property in the year it sold */
  ON B.PIN=CC.PIN AND YEAR(B.RECORDED_DATE)=CC.TAX_YEAR
    WHERE (1=1)
    and B.DEED_TYPE NOT IN ('Q', 'E', 'B') 
    AND MULT_IND NOT IN ('X') 
    AND YEAR(B.RECORDED_DATE)>=",min_sale_date, "
  ) AS INCEPTION
ON HD_PIN=INCEPTION.PIN 
LEFT JOIN
( /* For addresses, census tracts, and xy */
  SELECT * FROM PINLOCATIONS 
    INNER JOIN
    PROPLOCS
    ON LEFT(NAME, 10)=LEFT(PL_PIN, 10)
    WHERE primary_polygon IN (1) 
    AND PIN999 NOT IN (1)
  ) AS X
ON HD_PIN=PL_PIN
LEFT JOIN /* Want BOR numbers for prior year */
(SELECT PIN, LANDVAL, BLDGVAL FROM BAMASTER WHERE TAX_YEAR=2018) AS BOR
ON HD_PIN=BOR.PIN
LEFT JOIN/* Gives us the percentage of ownership for Condos */
  (SELECT DISTINCT DT_PIN, (TAX_YEAR+1) AS TAX_YEAR, DT_PER_ASS FROM DETAIL WHERE DT_CLASS=299) AS DETAIL 
  ON H.HD_PIN=DT_PIN AND DETAIL.TAX_YEAR=H.TAX_YEAR 
LEFT JOIN /* Gives us a count of the number of units in condo buildings */
  (SELECT COUNT(HD_PIN) AS n_units, PIN10, TAX_YEAR FROM 
    (SELECT DISTINCT HD_PIN, SUBSTRING(HD_PIN, 1, 10) AS PIN10, (TAX_YEAR+1) AS TAX_YEAR FROM HEAD) AS A
       GROUP BY PIN10, TAX_YEAR) AS AA
ON AA.PIN10=SUBSTRING(H.HD_PIN, 1, 10) AND H.TAX_YEAR=AA.TAX_YEAR
LEFT JOIN /* Allows us to proprate sale prices based on building size for multi properties */
  (SELECT SUM(BLDG_SF) as total_bldg_sf, PIN, TAX_YEAR FROM CCAOSFCHARS 
  WHERE MULTI_IND=1
  GROUP BY PIN, TAX_YEAR) AS PRO
ON PRO.PIN=H.HD_PIN AND PRO.TAX_YEAR=H.TAX_YEAR /* Note the slight difference here vs. the modeling data */
LEFT OUTER JOIN /*Excludes properties with a pending  inspection */
  (SELECT PIN, TAX_YEAR FROM PERMITTRACKING WHERE COMP_RECV=0) AS I
  ON I.PIN=H.HD_PIN AND I.TAX_YEAR=H.TAX_YEAR
WHERE (1=1)
  AND H.TAX_YEAR=2018
  AND primary_polygon IN (1)
  AND PIN999 NOT IN (1)
  AND HD_CLASS IN (211, 212, 200, 201, 241, 299, 202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295)
  AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN (",modeling_townships,")
ORDER BY HD_PIN, HD_CLASS 
"))
# , error = identity); result$message

# if current tax year is same also a  reassessment year for target_township, years_prior should be 4
# if current tax year is not a reassessment year for target_township, make sure to keep in consideration for which years
# 288s are valid when defining years_prior, i.e. a 288 can last up to 6 years depending on when it was filed
addchars <- dbGetQuery(CCAODATA, paste0("
SELECT QU_PIN as PIN, TAX_YEAR, QU_MLT_CD, QU_UPLOAD_DATE
  , QU_TYPE_OF_RES, QU_USE, QU_NUM_APTS, QU_EXTERIOR_WALL, QU_ROOF, QU_ROOMS, QU_BEDS, QU_BASEMENT_TYPE
  , QU_BASEMENT_FINISH, QU_HEAT, QU_AIR, QU_FIRE_PLACE, QU_ATTIC_TYPE, QU_ATTIC_FINISH, QU_FULL_BATH, QU_HALF_BATH
  , QU_TYPE_PLAN, QU_TYPE_DESIGN, QU_CONSTRUCT_QUALITY, QU_RENOVATION, QU_SITE_DESIRE, QU_GARAGE_SIZE
  , QU_GARAGE_CONST, QU_GARAGE_ATTACHED, QU_GARAGE_AREA, QU_PORCH, QU_SQFT_BLD, QU_STATE_OF_REPAIR
  , QU_NO__COM_UNIT
FROM ADDCHARS
WHERE QU_HOME_IMPROVEMENT = 1 AND TAX_YEAR > ", tax_year - 4
))

modeldata<-addchars_func(2)

print(Sys.time()-t)
integ_check_f(length(unique(modeldata[,c("PIN","DOC_NO", "MULTI_IND")]))==nrow(modeldata),
              paste0("Are there any duplicate multis?"),
              paste0("GOOD: All multis are unique"),
              paste0("Bad: ", nrow(modeldata)-nrow(unique(modeldata[,c("PIN","DOC_NO", "MULTI_IND")])), " multis are duplicates"),
              "0")
integ_check_f(length(unique(modeldata[,c("DOC_NO")]))==nrow(modeldata),
              paste0("Are there any duplicate deed numbers?"),
              paste0("GOOD: All deed numbers are unique"),
              paste0("Bad: ", nrow(modeldata)-length(unique(modeldata[,c("DOC_NO")])), " are duplicates"),
              "0")
integ_check_f(nrow(subset(modeldata, is.na(HD_SF)==TRUE))>0,
              paste0("Do any pins have pins have NULL land area"),
              paste0("Bad: ", nrow(subset(modeldata, is.na(HD_SF)==TRUE)), " pins have NULL land areas"),
              paste0("GOOD: no NULL land areas"),
              "0")

# Recode sale_price
modeldata$sale_price <- as.numeric(modeldata$sale_price)

# Recodes ----
  # Time variables
  # Arbitrarily use 2000 as a base year
modeldata$sale_date <- as.POSIXct(modeldata$sale_date, tz="America/Chicago")
modeldata$sale_date_n <- as.numeric(as.Date(modeldata$sale_date, format="%Y%m%d"))
modeldata$sale_date <- as.Date(modeldata$sale_date, format="%Y%m%d")
modeldata$sale_year <- lubridate::year(modeldata$sale_date)
modeldata$sale_quarter <- (modeldata$sale_year-2000)*4+as.numeric(as.character(substr(quarters(modeldata$sale_date),2,2)))
modeldata$sale_halfyear <- (modeldata$sale_year-2000)*2+car::recode(as.numeric(as.character(substr(quarters(modeldata$sale_date),2,2))), "1:2 = 1; 3:4 = 2")
modeldata$sale_quarterofyear <- as.numeric(as.character(substr(quarters(modeldata$sale_date),2,2)))
modeldata$sale_monthofyear <-as.numeric(format(modeldata$sale_date, "%m"))
modeldata$sale_halfofyear <- ifelse(as.numeric(as.character(substr(quarters(modeldata$sale_date),2,2)))<=2,1,2)
modeldata <- e.recode$fac(modeldata, c("sale_halfofyear", "sale_monthofyear", "sale_quarterofyear"))

# Proration ----
modeldata$HD_SF <- ifelse(modeldata$MULTI_IND==1 & modeldata$modeling_group!='NCHARS' & !is.na(modeldata$total_bldg_sf),
                          modeldata$HD_SF*modeldata$BLDG_SF/modeldata$total_bldg_sf
                          ,modeldata$HD_SF) 
modeldata$sale_price <- ifelse(modeldata$MULTI_IND==1 & modeldata$modeling_group!='NCHARS' & !is.na(modeldata$total_bldg_sf),
                            modeldata$BLDG_SF/modeldata$total_bldg_sf*modeldata$sale_price,
                            modeldata$sale_price)
# Identify the most recent sale
  # NEED MORE EFFICIENT 
for (i in 1:length(modeldata$PIN)){
  # pb$tick()
  # Sys.sleep(1 / bootstrapp_iters)
  
  if(max(subset(modeldata, PIN==modeldata$PIN[i])$sale_date)==modeldata$sale_date[i]){
    modeldata$most_recent_sale[i] <-1
  }else{modeldata$most_recent_sale[i] <-0}
}
  
# Numeric variables ----
  # SAME THING HERE WE WANT TO LOOP OVER A LIST
modeldata <- e.recode$num(modeldata, c("centroid_x", "centroid_y", "sale_price", "BLDG_SF", "FBATH", "HBATH", "AGE"))
modeldata$AGE_SQRD<-as.numeric(modeldata$AGE)^2
modeldata$AGE_DECADE<-round(as.numeric(modeldata$AGE)/10,1)
modeldata$AGE_DECADE_SQRD<-round(as.numeric(modeldata$AGE)/10,1)^2
modeldata$ROOMS[modeldata$ROOMS==0]<-NA
modeldata$centroid_x_demeaned<-(modeldata$centroid_x-mean(modeldata$centroid_x))
modeldata$centroid_y_demeaned<-(modeldata$centroid_y-mean(modeldata$centroid_y))

# Initial filter ----
# (user defined exclusions due to data issues, observations with filter_1 value of "1" will be used) 
modeldata$puremarket <- 0
modeldata$puremarket<-ifelse(
  (modeldata$modeling_group!="NCHARS" 
   & modeldata$sale_price>100 
  & is.finite(modeldata$sale_price)
  & modeldata$BLDG_SF>100 & is.finite(modeldata$BLDG_SF)) |
    (modeldata$modeling_group=="NCHARS" & modeldata$sale_price>100 
  & is.finite(modeldata$sale_price))
  ,1,0)
modeldata$filter_1 <- modeldata$puremarket

# Location factor ----
# Check to make sure all PINs have xy data
pins_missing_xy <- subset(modeldata[,c("PIN", "centroid_x", "centroid_y")], is.na(centroid_x))
write.table(pins_missing_xy, paste0(dirs$bad_data, "pins_missing_xy.txt"), sep = "|", na = "", quote = FALSE)
integ_check_f(nrow(pins_missing_xy)>0,
              paste0("All PINs should be on the Clerk's shape file"),
              paste0("Bad: ", nrow(pins_missing_xy), " PINs are missing xy data"),
              "GOOD: All PINs have xy coordinates",
              "0")
# previous integrity check only created missing pin table when pins were missing
# if (nrow(pins_missing_xy)>0){
#   check<- paste0("All PINs should be on the Clerk's shape file")
#   msg<- paste0("Bad: ", nrow(pins_missing_xy), " PINs are missing xy data")
#   integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
#   write.table(pins_missing_xy, paste0(dirs$bad_data, "pins_missing_xy.txt"), sep = "|", na = "", quote = FALSE)
#   print(check); print(msg); rm(check, msg)
# }else{
#   check<- paste0("All PINs should be on the Clerk's shape file")
#   msg<- "GOOD: All PINs have xy coordinates"
#   integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
#   print(check); print(msg); rm(check, msg)
# }
rm(pins_missing_xy)

# Estimate the location model
LOCF_MODEL <- gam(sale_price~
  s(centroid_x)
  +s(centroid_y)
  +s(centroid_x,centroid_y)
  +sale_date+sale_quarterofyear
  ,data=subset(modeldata, filter_1>=1), na.action = na.exclude)

# Estimate by forcing sale date to be the date we want to estimate at
this <- subset(modeldata[, c("DOC_NO","centroid_x", "centroid_y", "sale_date", "sale_quarterofyear","sale_year","sale_monthofyear" ,"sale_price","filter_1")], 
               !is.na(sale_date) & !is.na(centroid_x) & !is.na(centroid_y))
this$saledate <- estimation_date
this$sale_quarterofyear <-lubridate::quarter(estimation_date) # MAKE SURE that you are using the same time variables as the LOCF_MODEL
this$LOC_PRED <- as.numeric(predict(LOCF_MODEL,newdata=this));
this$LOCF1 <- this$LOC_PRED/mean(this$LOC_PRED)

# Validity check
integ_check_f(min(this$LOCF1)>0,
              paste0("Location factors must always be positive"),
              paste0("Good: LOCF always positive"),
              "BAD: LOCF has negative values",
              "0")
integ_check_f(mean(this$LOCF1)==1,
              paste0("Location factors must always have a mean ==1"),
              paste0("Good: LOCF has a mean of 1"),
              "BAD: LOCF not mean 1",
              "0")
integ_check_f(length(unique(this$PIN))!=length(unique(this$PIN, this$LOCF1)),
              paste0("Location factors must be constant within PIN"),
              paste0("Bad: Same PIN, different location factors"),
              paste0("Good: Same PIN, same location factor"),
              "0")
modeldata <- join(this[,c("DOC_NO", "LOCF1")], modeldata)
rm(this)

# Factor variables ----
# factors <- c("TOWN_CODE") HELP GET THIS WORKING
# for(f in 1:length(factors)){
#   eval(modeldata$factors[f])<-factor(eval(modeldata$factors[f]))
# }
modeldata$MULTI_FAMILY_IND<- factor(modeldata$MULTI_FAMILY_IND)
modeldata$NBHD<-factor(modeldata$NBHD)
#modeldata$TRACTCE<-factor(modeldata$TRACTCE)

modeldata$TOWN_CODE<-factor(modeldata$TOWN_CODE)
modeldata$FRPL<-factor(ifelse(modeldata$FRPL>=2, 2, modeldata$FRPL))
modeldata$EXT_WALL<-as.numeric(factor(car::recode(modeldata$EXT_WALL, "0=NA")))
modeldata$EXT_WALL<-as.factor(modeldata$EXT_WALL)
modeldata$GARAGE_IND <- factor(ifelse(modeldata$GAR1_SIZE==7,0,1))
modeldata$CLASS<-as.factor(modeldata$CLASS)
modeldata$sale_halfofyear <- factor(modeldata$sale_halfofyear)
modeldata$sale_monthofyear <- factor(modeldata$sale_monthofyear)
modeldata$sale_quarterofyear <- factor(modeldata$sale_quarterofyear)
modeldata$TRACTCE<-factor(modeldata$TRACTCE)
modeldata$CONDO_CLASS_FACTOR<-as.factor(modeldata$CONDO_CLASS_FACTOR)
modeldata$BSMT_FIN<-factor(ifelse(modeldata$BSMT_FIN==0, NA, modeldata$BSMT_FIN))
modeldata$BSMT[modeldata$BSMT==0]<-NA
modeldata$BSMT<-factor(modeldata$BSMT)
modeldata$AIR[modeldata$AIR==0]<-NA
modeldata$AIR[modeldata$AIR==2]<-0
modeldata$AIR <- factor(modeldata$AIR)
modeldata$ROOF_CNST<-factor(ifelse(modeldata$ROOF_CNST==0, NA, modeldata$ROOF_CNST))
modeldata$HEAT<-factor(car::recode(modeldata$HEAT, "0=NA; 3:4=2"))
modeldata$BEDS[modeldata$BEDS==0]<-NA
modeldata$OHEAT<-factor(modeldata$OHEAT)
modeldata$ATTIC_TYPE<-factor(ifelse(modeldata$ATTIC_TYPE==0, NA, modeldata$ATTIC_TYPE))
modeldata$CNST_QLTY<-factor(ifelse(modeldata$CNST_QLTY==0, NA, modeldata$CNST_QLTY))
modeldata$SITE<-factor(ifelse(modeldata$SITE==0, NA, modeldata$SITE))
modeldata$GAR1_SIZE <- car::recode(modeldata$GAR1_SIZE, "7=0; 8=7")
modeldata <- e.recode$fac(modeldata, c("GAR1_CNST", "GAR1_ATT", "GAR1_AREA", "PORCH"))
modeldata$REPAIR_CND<-factor(ifelse(modeldata$REPAIR_CND==0, NA, modeldata$REPAIR_CND))

# Geographic recodes ----
modeldata <- e.recode$num(modeldata, c("TOWN_CODE", "NBHD", "TRACTCE"))

modeldata$NBHD_mapping <- modeldata$NBHD

# Variables for legacy model
modeldata <- legacy_recodes(modeldata)

# generate combined neighborhood/towncode variable specific to modeling groups to avoid having to use interaction terms
modeldata$town_nbhd <- as.factor(paste0(modeldata$TOWN_CODE, modeldata$NBHD))

modeldata$modeling_group <- as.factor(modeldata$modeling_group)

for (i in levels(modeldata$modeling_group)){
  eval(parse(text = paste0("modeldata$town_nbhd_", i, " <- modeldata$town_nbhd")))
  eval(parse(text = paste0("modeldata$town_nbhd_", i, "[modeldata$modeling_group!='", i, "']<-NA")))
  eval(parse(text = paste0("modeldata$town_nbhd_", i, "<-droplevels(modeldata$town_nbhd_", i, ")")))
}

# Want as factor once geographic recodes are completed
modeldata <- e.recode$fac(modeldata, c("TOWN_CODE", "NBHD", "TRACTCE"))

#Add price tercile variable
modeldata$price_tercile <- NA
tercile_info <- NULL

for(m in levels(modeldata$modeling_group)){
  onethird <- quantile(modeldata$sale_price[modeldata$modeling_group == m & 
                                              !is.na(modeldata$sale_price) & 
                                              modeldata$sale_price>100], c(.33), names = FALSE, na.rm = TRUE)
  twothird <- quantile(modeldata$sale_price[modeldata$modeling_group == m &
                                              !is.na(modeldata$sale_price) &
                                              modeldata$sale_price>100], c(.66), names = FALSE, na.rm = TRUE)
  modeldata$price_tercile[modeldata$sale_price <= onethird & modeldata$modeling_group == m] <- 1
  modeldata$price_tercile[modeldata$sale_price > onethird & modeldata$sale_price <= twothird & modeldata$modeling_group == m] <- 2
  modeldata$price_tercile[modeldata$sale_price > twothird & modeldata$modeling_group == m] <- 3 
  tercile_info <- rbind(tercile_info, c(m, onethird, twothird))
}
modeldata$price_tercile[modeldata$sale_price<=100] <- NA
modeldata$price_tercile <- factor(modeldata$price_tercile)
modeldata$price_tercile <- ordered(modeldata$price_tercile, c(1, 2, 3))
colnames(tercile_info) <- c("modeling_group", "lower_third_cutoff", "upper_third_cutoff")

#tercile integrity checks
m_tercile_integ <- modeldata[,-10] %>% 
  dplyr::group_by(modeling_group, price_tercile) %>% 
  dplyr::summarise(count = n(), median.sale.price = median(sale_price, na.rm=TRUE))

#create an ordered logit model to predict price tercile in unsold data
#Specify model parameters:
tercile_models <- read.xlsx("ccao_dictionary_draft.xlsx", sheetName = "price_tercile_model", stringsAsFactors=FALSE)
tercile_models <- tercile_models[tercile_models$township == target_township, ]
sfmod <- tercile_models$sf_probit_model
mfmod <- tercile_models$mf_probit_model
#ncmod <- tercile_models$nchars_probit_model

err <- try(TERC_MOD_SF <- polr(formula = sfmod, data = modeldata, subset= modeldata$modeling_group=="SF", method = "probit", na.action = na.omit))
if(class(err)!="try-error"){
  TERC_MOD_SF <- polr(formula = sfmod, data = modeldata, subset= modeldata$modeling_group=="SF", method = "probit", na.action = na.omit)
}
integ_check_f(class(err)!="try-error",
              paste0("Price Tercile Probit Model for SF"),
              paste0("Good: Model Created for SF"),
              paste0("Bad: Model Not Created for SF"),
              "0")

err <- try(TERC_MOD_MF <- polr(formula = mfmod, data = modeldata, subset= modeldata$modeling_group=="MF", method = "probit", na.action = na.omit))
if(class(err)!="try-error"){
  TERC_MOD_MF <- polr(formula = mfmod, data = modeldata, subset= modeldata$modeling_group=="MF", method = "probit", na.action = na.omit)
}
integ_check_f(class(err)!="try-error",
              paste0("Price Tercile Probit Model for MF"),
              paste0("Good: Model Created for MF"),
              paste0("Bad: Model Not Created for MF"),
              "0")
# previous integrity check only created tercile probit model for mf when condition met
# if(class(err)!="try-error"){
#   TERC_MOD_MF <- polr(formula = mfmod, data = modeldata, subset= modeldata$modeling_group=="MF", method = "probit", na.action = na.omit)
#   check<- paste0("Price Tercile Probit Model for MF")
#   msg<- paste0("Good: Model Created for MF")
#   integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
#   print(check); print(msg); rm(check, msg)
# }else{
#   check<- paste0("Price Tercile Probit Model for MF")
#   msg<- paste0("Bad: Model Not Created for MF")
#   integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
#   print(check); print(msg); rm(check, msg)
# }

#err <- try(TERC_MOD_NCHARS <- polr(formula = ncmod, data = modeldata, subset= modeldata$modeling_group=="NCHARS", method = "probit", na.action = na.omit))
#if(class(err)!="try-error"){
#  TERC_MOD_NCHARS <- polr(formula = ncmod, data = modeldata, subset= modeldata$modeling_group=="NCHARS", method = "probit", na.action = na.omit)
#  check<- paste0("Price Tercile Probit Model for NCHARS")
#  msg<- paste0("Good: Model Created for NCHARS")
#  integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
#  print(check); print(msg); rm(check, msg)
#}else{
#  check<- paste0("Price Tercile Probit Model for NCHARS")
#  msg<- paste0("Bad: Model Not Created for NCHARS")
#  integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
#  print(check); print(msg); rm(check, msg)
#}

remove(sfmod, mfmod) #need to add ncmod to this list if using NCHARS model


#Check tercile prediction accuracy: 
err <- try(predict(TERC_MOD_SF, newdata = subset(modeldata, subset = modeldata$modeling_group == "SF")))
if(class(err)!="try-error"){
  modeldata$price_tercile_pred[modeldata$modeling_group=="SF"] <- predict(TERC_MOD_SF, newdata = subset(modeldata, subset = modeldata$modeling_group == "SF"))
}
err <-try(predict(TERC_MOD_MF, newdata = subset(modeldata, subset = modeldata$modeling_group == "MF")))
if(class(err)!="try-error"){
  modeldata$price_tercile_pred[modeldata$modeling_group=="MF"] <- predict(TERC_MOD_MF, newdata = subset(modeldata, subset = modeldata$modeling_group == "MF"))
}
#err <- try(predict(TERC_MOD_NCHARS, newdata = subset(modeldata, subset = modeldata$modeling_group == "NCHARS")))
#if(class(err)!="try-error"){
#  modeldata$price_tercile_pred[modeldata$modeling_group=="NCHARS"] <- predict(TERC_MOD_NCHARS, newdata = subset(modeldata, subset = modeldata$modeling_group == "NCHARS"))
#}
modeldata$price_tercile_pred <- ordered(modeldata$price_tercile_pred, c(1, 2, 3))
modeldata$price_tercile_pred[modeldata$sale_price<=100] <- NA

ptacc <- round(mean((modeldata$price_tercile == modeldata$price_tercile_pred), na.rm = TRUE)*100)
pt1acc <- round(mean((modeldata$price_tercile[modeldata$price_tercile=="1"] == modeldata$price_tercile_pred[modeldata$price_tercile=="1"]), na.rm = TRUE)*100)
pt2acc <- round(mean((modeldata$price_tercile[modeldata$price_tercile=="2"] == modeldata$price_tercile_pred[modeldata$price_tercile=="2"]), na.rm = TRUE)*100)
pt3acc <- round(mean((modeldata$price_tercile[modeldata$price_tercile=="3"] == modeldata$price_tercile_pred[modeldata$price_tercile=="3"]), na.rm = TRUE)*100)

check<- paste0("Accuracy for price tercile probit model")
msg<- paste0(" Overall: ",ptacc, "% 1st tercile: ", pt1acc, "% 2nd tercile: ", pt2acc, "% 3rd tercile: ", pt3acc, "%")
integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
print(check); print(msg)
rm(check, msg, ptacc, pt1acc, pt2acc, pt3acc, err, onethird, twothird)

#Uncomment this code to create a plot to visualize price tercile prediction on model data: 
#tercile_info<-as.data.frame(tercile_info)
#sflow <- as.numeric(as.character(tercile_info[3,2]))
#sfhigh <- as.numeric(as.character(tercile_info[3,3]))
#mflow <- as.numeric(as.character(tercile_info[1,2]))
#mfhigh <- as.numeric(as.character(tercile_info[1,3]))
##include the following lines for nchar info only:
##nclow <- as.numeric(as.character(tercile_info[2,2]))
##nchigh <- as.numeric(as.character(tercile_info[2,3])) 

#ggplot(data = subset(modeldata, !is.na(price_tercile_pred)), aes(x = sale_price)) + 
#  geom_density(aes(fill=price_tercile_pred), position="fill") + 
#  coord_cartesian(xlim=c(0, 1500000)) + scale_color_brewer(palette="Set2") +
#  labs(title = "Density Plot of Price Tercile Prediction", x = "Sale Price") +
#  theme(legend.position="bottom") +
#  geom_vline(xintercept= c(sflow, sfhigh), linetype="dashed", color = "red") +
#  geom_vline(xintercept= c(mflow, mfhigh), linetype="longdash", color = "royalblue") +
#  #geom_vline(xintercept= c(nclow, nchigh), linetype="dotted", color = "magenta") + #include this line for nchars only
#  annotation_custom(grob=(grid.text("Red lines = SF Terciles; Blue Lines = MF terciles", x=.7, y=.2, gp=gpar(col="black", fontsize=11))))
#remove(sflow, sfhigh, mflow, mfhigh) #add nclow, nchigh to this list if using nchars

modeldata$price_tercile_pred <- NULL

# legacy weights Dave uses
for(t in levels(modeldata$TOWN_CODE)){
  for(n in levels(modeldata$NBHD)){
    modeldata$legacy_weight[modeldata$TOWN_CODE==t & modeldata$NBHD==n] <-
      median(subset(modeldata, !is.na(sale_price) & TOWN_CODE==t & NBHD==n)$sale_price)/100000
  }
}

# neighborhood recode experiment
if (nbhd_experiment == 'on'){
  recoded_pins <- read.xlsx("O:/CCAODATA/data/specific_pin_groups/ReClass NT draft.xlsx", sheetName = "Sheet1", stringsAsFactors=FALSE)[,1]
  recoded_pins <- paste0(0, recoded_pins)
  modeldata <- nbhd_recode_func(modeldata, recoded_pins, 171, 80)
}else {
  recoded_pins <- NA
}

# problematic pins excluded, usually due to missing data
modeldata <- subset(modeldata, !(modeldata$PIN %in% read.xlsx("ccao_dictionary_draft.xlsx", sheetName = "pin_exclusions", stringsAsFactors=FALSE)[,1]))

# Labeling ----
# TO DO
# Label the data frame
# Label fields
# label values

integ_check_f(length(unique(modeldata[,c("DOC_NO")]))==nrow(modeldata),
              paste0("Are there any duplicate deed numbers?"),
              paste0("GOOD: All deed numbers are unique"),
              paste0("Bad: ", nrow(modeldata)-length(unique(modeldata[,c("DOC_NO")])), " are duplicates"),
              "0")

file_info <- update_file_info('0_build_200class_modeldata')
# Save data ----
save(dirs, integrity_checks, file_info
     , modeldata, LOCF_MODEL, recoded_pins, TERC_MOD_SF, TERC_MOD_MF
     , file=destfile)