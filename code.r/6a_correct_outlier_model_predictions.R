# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/. 

# Top comments ----

# This script allows an analyst to amend outliers detected during modeling by updating incorrect characteristics
# and running the pin back through the model with corrected information.

load(destfile)

# function for quickly correcting outliers during desktop review
outlier_correction <- function(pin, fields, new_values) {
  
  # example use case
  # outlier_corrections <- valuation_data
  # outlier_corrections <- outlier_correction("05072130360000", c("FBATH", "HBATH"), c("5", "2"))
  
  n <- length(fields)
  for (i in 1:n) {
    col <- which(colnames(outlier_corrections)==fields[i])
    row <- which(outlier_corrections$PIN==pin)
    outlier_corrections[row,col] <- as.numeric(new_values[i])
    outlier_corrections$fields_adjusted[outlier_corrections$PIN==pin] <- toString(fields)
    outlier_corrections$new_values[outlier_corrections$PIN==pin] <- toString(new_values)
  }
  return(outlier_corrections)
}

outlier_corrections <- valuationdata

# calculate mean percentage difference by class and neighborhood
for (i in levels(outlier_corrections$NBHD)) {
  
  for (c in levels(outlier_corrections$CLASS)) {
    
    outlier_corrections$ave_fitted_diff[outlier_corrections$NBHD == i & outlier_corrections$CLASS == c] <- 1 - (mean(outlier_corrections$fitted_value_6[outlier_corrections$NBHD == i & outlier_corrections$CLASS == c] / outlier_corrections$fitted_value_1[outlier_corrections$NBHD == i & outlier_corrections$CLASS == c]))
    
  }
  
}

outlier_pins <- c("09201200070000"
                , "09252080520000"
                 )

outlier_fields <- list(c("BLDG_SF")
                     , c("BLDG_SF")
                      )

outlier_changes <- list(c("2002")
                      , c("1315")
                       )

# create subset of VALUATIONDATA only including those pins
outlier_corrections <- subset(outlier_corrections, PIN %in% outlier_pins)
outlier_corrections$fields_adjusted <- NA
outlier_corrections$new_values <- NA

# loop through pins and apply changes
for (t in 1:length(outlier_pins)){
  outlier_corrections <- outlier_correction(outlier_pins[[t]], outlier_fields[[t]], outlier_changes[[t]])
}

# predict new values after corrected erroneous fields
outlier_corrections$corrected_fitted_value_1 <- exp(predict(final_sf_model, newdata=outlier_corrections))
outlier_corrections$corrected_fitted_value_6 <- outlier_corrections$corrected_fitted_value_1*(1-outlier_corrections$ave_fitted_diff)
outlier_corrections$net_change <- outlier_corrections$corrected_fitted_value_6-outlier_corrections$fitted_value_6

# print output for most recently added pin
outlier_corrections$output <- paste0("original fitted value for PIN ", outlier_corrections$PIN, " was $", formatC(round(outlier_corrections$fitted_value_6, 0), format="d", big.mark=","), ", corrected predicted value is $", formatC(round(outlier_corrections$corrected_fitted_value_6, 0), format="d", big.mark=","))
print(outlier_corrections$output[outlier_corrections$PIN == outlier_pins[length(outlier_pins)]])

if (length(outlier_pins) == length(outlier_fields) & length(outlier_fields) == length(outlier_changes)){
  print("input lists still have equal lengths, you haven't bungled it yet")
  
  # simplify data frame
  outlier_corrections <- dplyr::select(outlier_corrections, c("PIN", "fitted_value_1","fitted_value_6", "corrected_fitted_value_1", "ave_fitted_diff", "corrected_fitted_value_6", "net_change", "TOWN_CODE", "NBHD", "ADDR", "CLASS", "fields_adjusted", "new_values"))
  
  # export table
  write.table(outlier_corrections, file = paste0("O:/CCAODATA/data/bad_data/corrected_outliers_", target_township, "_2019.csv"), row.names = FALSE, sep = ",")
  
}else{
  print("ERROR | input lists are NOT EQUAL; you've bungled it, good work")
}

save(dirs, integrity_checks, file_info
     , modeldata, assmntdata, valuationdata, LOCF_MODEL
     , models, modeling_results
     , final_sf_model, final_mf_model, final_nchars_model, best_models
     , recoded_pins, TERC_MOD_SF, TERC_MOD_MF
     , outlier_corrections
     , file=destfile)